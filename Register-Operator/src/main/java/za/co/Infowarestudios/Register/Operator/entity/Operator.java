package za.co.Infowarestudios.Register.Operator.entity;

import javax.persistence.*;


/**
 * Created by SourceCode on 09/Dec/2015.
 */
@Entity
@Table(name = "Operators")
public class Operator {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "Operator_FirstName")
    private String firstName;

    @Column(name = "Operator_LastName")
    private String lastName;

    @Column(name = "Operator_Id")
    private String operatorId;

    @Column(name = "Operator_ID_Or_Passport")
    private String idOrPassport;

    @Column(name = "Operator_Sim_Number")
    private String iccid;

    @Column(name = "Operator_Serial_Number")
    private String serialNumber;

    @Column(name = "Operator_CellNumber")
    private String cellNumber;

    @Column(name = "Operator_HomeNumber")
    private String homeNumber;

    @Column(name = "Operator_Work_Number")
    private String workNumber;

    @Column(name = "Operator_StreetAddress")
    private String streetAddress;

    @Column(name = "Operator_Suburb")
    private String suburb;

    @Column(name = "Operator_CityOrTown")
    private String cityOrTown;

    @Column(name = "Operator_Code")
    private String code;

    @Column(name = "Operator_Email_Address")
    private String email;

    @Column(name = "Operator_Network")
    private String network;

    @Column(name = "Network_ID")
    private String networkId;

    @Column(name = "selpal_cell_Number")
    private String selpalCellNumber;

    @Column(name = "Operator_Nationality")
    private String nationality;

    @Column(name = "Operator_Ethnic_Group")
    private String ethicGroup;

    @Column(name = "Operator_Preferred_Language")
    private String preferredLanguage;

    @Column(name = "Operator_Marital_Status")
    private String maritalStatus;

    @Column(name = "Operator_Residence")
    private String residence;

    @Column(name = "Operator_Shop_Address")
    private String shopAddress;

    @Column(name = "Sales_rep_ID")
    private String salesRepId;

    @Column(name = "Operator_Copy_Of_ID")
    private String copyOfId;

    @Column(name = "Operator_Proof_Of_Residence")
    private String proofOfResidence;

    @Column(name = "Operator_User_Code")
    private String userCode;

    @Column(name = "PIN")
    private String pin;

    protected Operator() {

    }

    public Operator(String firstName, String lastName, String operatorId, String idOrPassport, String iccid,
                    String serialNumber, String cellNumber, String homeNumber,
                    String workNumber, String streetAddress, String suburb,
                    String cityOrTown, String code, String email, String network,
                    String networkId, String selpalCellNumber,
                    String nationality, String ethicGroup, String preferredLanguage,
                    String maritalStatus, String residence, String shopAddress,
                    String salesRepId, String copyOfId, String proofOfResidence,
                    String userCode, String pin) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.operatorId = operatorId;
        this.idOrPassport = idOrPassport;
        this.iccid = iccid;
        this.serialNumber = serialNumber;
        this.cellNumber = cellNumber;
        this.homeNumber = homeNumber;
        this.workNumber = workNumber;
        this.streetAddress = streetAddress;
        this.suburb = suburb;
        this.cityOrTown = cityOrTown;
        this.code = code;
        this.email = email;
        this.network = network;
        this.networkId = networkId;
        this.selpalCellNumber = selpalCellNumber;
        this.nationality = nationality;
        this.ethicGroup = ethicGroup;
        this.preferredLanguage = preferredLanguage;
        this.maritalStatus = maritalStatus;
        this.residence = residence;
        this.shopAddress = shopAddress;
        this.salesRepId = salesRepId;
        this.copyOfId = copyOfId;
        this.proofOfResidence = proofOfResidence;
        this.userCode = userCode;
        this.pin = pin;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public void generateOperatorId(int index) {

        String prefix = "" + firstName.charAt(0) + lastName.charAt(0) + lastName.charAt(1);
        this.operatorId = prefix.toUpperCase() + String.format("%05d", index);
    }

    public String getLastName() {

        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public String getIdOrPassport() {

        return idOrPassport;
    }

    public void setIdOrPassport(String idOrPassport) {
        this.idOrPassport = idOrPassport;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {

        this.iccid = iccid;
    }

    public String getSerialNumber() {

        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {

        this.serialNumber = serialNumber;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {

        this.cellNumber = cellNumber;
    }

    public String getHomeNumber() {

        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getWorkNumber() {

        return workNumber;
    }

    public void setWorkNumber(String workNumber) {

        this.workNumber = workNumber;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {

        this.suburb = suburb;
    }

    public String getCityOrTown() {

        return cityOrTown;
    }

    public void setCityOrTown(String cityOrTown) {

        this.cityOrTown = cityOrTown;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {

        this.code = code;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getNetwork() {

        return network;
    }

    public void setNetwork(String network) {

        this.network = network;
    }

    public String getNetworkId() {
        return networkId;
    }

    public void setNetworkId(String networkId) {

        this.networkId = networkId;
    }

    public String getSelpalCellNumber() {

        return selpalCellNumber;
    }

    public void setSelpalCellNumber(String selpalCellNumber) {

        this.selpalCellNumber = selpalCellNumber;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {

        this.nationality = nationality;
    }

    public String getEthicGroup() {

        return ethicGroup;
    }

    public void setEthicGroup(String ethicGroup) {

        this.ethicGroup = ethicGroup;
    }

    public String getPreferredLanguage() {

        return preferredLanguage;
    }

    public void setPreferredLanguage(String preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getResidence() {
        return residence;
    }

    public void setResidence(String residence) {
        this.residence = residence;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getSalesRepId() {
        return salesRepId;
    }

    public void setSalesRepId(String salesRepId) {

        this.salesRepId = salesRepId;
    }

    public String getCopyOfId() {
        return copyOfId;
    }

    public void setCopyOfId(String copyOfId) {

        this.copyOfId = copyOfId;
    }

    public String getProofOfResidence() {

        return proofOfResidence;
    }

    public void setProofOfResidence(String proofOfResidence) {

        this.proofOfResidence = proofOfResidence;
    }

    public String getUserCode() {

        return userCode;
    }

    public void setUserCode(String userCode) {

        this.userCode = userCode;
    }

    public String getPin() {

        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public void setId(long id) {
        this.id = id;
    }

}










