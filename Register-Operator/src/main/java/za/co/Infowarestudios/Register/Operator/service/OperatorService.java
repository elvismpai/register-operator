package za.co.Infowarestudios.Register.Operator.service;

import org.springframework.beans.factory.annotation.Autowired;
import za.co.Infowarestudios.Register.Operator.entity.Operator;
import za.co.Infowarestudios.Register.Operator.exceptions.OperatorNotFoundException;
import za.co.Infowarestudios.Register.Operator.repository.OperatorRepository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by SourceCode on 09/Dec/2015.
 */

public class OperatorService {

    @Autowired
    private OperatorRepository operatorRepository;

    public Operator createOperator(final Operator operator){
        return operatorRepository.save(operator);
    }

    public Operator updateOperator(Operator operator) throws OperatorNotFoundException{
       if(operatorRepository.exists(operator.getId())){
            return createOperator(operator);
        } else {
            throw new OperatorNotFoundException("Operator cannot be updated. Not found");
        }
    }
    public Operator getOperatorById(final long id) throws OperatorNotFoundException {
        Operator operator = operatorRepository.findOne(id);
        if (operator == null) {
            throw new OperatorNotFoundException("Operator does not exist");
        } else {
            return operator;
        }
    }
    public List<Operator>getAllOperators(){
        Iterator iterator = operatorRepository.findAll().iterator();
        List<Operator> op = new ArrayList<Operator>();
        while(iterator.hasNext()){
            Operator o = (Operator)iterator.next();
            op.add(o);
        }
        return op;
    }

}
