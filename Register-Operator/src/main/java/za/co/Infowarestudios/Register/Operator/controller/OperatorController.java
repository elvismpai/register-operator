package za.co.Infowarestudios.Register.Operator.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.Infowarestudios.Register.Operator.entity.Operator;
import za.co.Infowarestudios.Register.Operator.exceptions.OperatorNotFoundException;
import za.co.Infowarestudios.Register.Operator.service.OperatorService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SourceCode on 2015/12/17.
 */
@RestController
@RequestMapping("/operators")
public class OperatorController {
    private static final Logger logger = LoggerFactory.getLogger(OperatorController.class);

    @Autowired
    private OperatorService service;

    @RequestMapping(value = "", method = RequestMethod.PUT)
    public ResponseEntity createOperator(@RequestBody String json) {
        try {
            ObjectMapper mapper = new ObjectMapper();

            final JsonNode data = mapper.readTree(json);

            String name = data.get("firstName").asText();
            String surname = data.get("lastName").asText();
            String operatorID = data.get("operatorId").asText();
            String Id = data.get("idOrPassport").asText();
            String Iccid = data.get("iccid").asText();
            String serialNumber = data.get("serialNumber").asText();
            String cellNumber = data.get("cellNumber").asText();
            String homeNumber = data.get("homeNumber").asText();
            String WorkNumber = data.get("workNumber").asText();
            String StreetAddress = data.get("streetAddress").asText();
            String Suburb = data.get("suburb").asText();
            String CityOrTown = data.get("cityOrTown").asText();
            String Code = data.get("code").asText();
            String email = data.get("email").asText();
            String Network = data.get("network").asText();
            String networkId = data.get("networkId").asText();
            String selpalCellNumber = data.get("selpalCellNumber").asText();
            String nationality = data.get("nationality").asText();
            String ethicGroup = data.get("ethicGroup").asText();
            String preferredLanguage = data.get("preferredLanguage").asText();
            String maritalStatus = data.get("maritalStatus").asText();
            String residence = data.get("residence").asText();
            String shopAddress = data.get("shopAddress").asText();
            String salesRepId = data.get("salesRepId").asText();
            String copyOfId = data.get("copyOfId").asText();
            String proofOfResidence = data.get("proofOfResidence").asText();
            String userCode = data.get("userCode").asText();
            String pin = data.get("pin").asText();
            Operator operatorToAdd =new Operator(name, surname, operatorID,
                    Id, Iccid, serialNumber, cellNumber, homeNumber, WorkNumber,
                    StreetAddress, Suburb, CityOrTown, Code, email,
                    Network, networkId, selpalCellNumber, nationality, ethicGroup,
                    preferredLanguage, maritalStatus, residence, shopAddress, salesRepId, copyOfId,
                    proofOfResidence, userCode, pin);



            List<Operator> commonOperator= new ArrayList<Operator>();

            logger.info("after initialising common");
            List<Operator> operatorList = service.getAllOperators();
            logger.info("after getting from the database common");
            logger.info("list size ="+ operatorList.size());
            int commonCount = 0;
            if(operatorList !=null){
                logger.info("inside the if statement");
                // logger.info("inside the if statement");
                for (int i = 0; i < operatorList.size(); i++) {
                    Operator op = operatorList.get(i);
                    String prefix = ""+name.charAt(0)+surname.charAt(0)+surname.charAt(1);
                    if(op.getOperatorId().contains(prefix.toUpperCase())){
                        commonOperator.add(op);
                    }
                }
                commonCount = commonOperator.size();
            }
            logger.info("common count is :" + commonCount);
            operatorToAdd.generateOperatorId(commonCount + 1);
            service.createOperator(operatorToAdd);
            return new ResponseEntity(HttpStatus.CREATED);

        } catch (IOException e) {

            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public ResponseEntity updateOperator(@RequestBody String json) throws OperatorNotFoundException {
        try
        {
            ObjectMapper mapper = new ObjectMapper();

            final JsonNode data = mapper.readTree(json);

            logger.info("Received json string"+json);

            long operatorId = data.get("id").asLong();
            String name = data.get("firstName").asText();
            String surname = data.get("lastName").asText();
            String operatorID = data.get("operatorId").asText();
            String Id = data.get("idOrPassport").asText();
            String Iccid = data.get("iccid").asText();
            String serialNumber = data.get("serialNumber").asText();
            String cellNumber = data.get("cellNumber").asText();
            String homeNumber = data.get("homeNumber").asText();
            String WorkNumber = data.get("workNumber").asText();
            String StreetAddress = data.get("streetAddress").asText();
            String Suburb = data.get("suburb").asText();
            String CityOrTown = data.get("cityOrTown").asText();
            String Code = data.get("code").asText();
            String email = data.get("email").asText();
            String Network = data.get("network").asText();
            String networkId = data.get("networkId").asText();
            String selpalCellNumber = data.get("selpalCellNumber").asText();
            String nationality = data.get("nationality").asText();
            String ethicGroup = data.get("ethicGroup").asText();
            String preferredLanguage = data.get("preferredLanguage").asText();
            String maritalStatus = data.get("maritalStatus").asText();
            String residence = data.get("residence").asText();
            String shopAddress = data.get("shopAddress").asText();
            String salesRepId = data.get("salesRepId").asText();
            String copyOfId = data.get("copyOfId").asText();
            String proofOfResidence = data.get("proofOfResidence").asText();
            String userCode = data.get("userCode").asText();
            String pin = data.get("pin").asText();

            if(data.get("id").asLong() == 0L){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

            Operator operator = new Operator(name, surname,operatorID,
                    Id, Iccid, serialNumber, cellNumber,homeNumber, WorkNumber,
                    StreetAddress, Suburb, CityOrTown, Code, email,
                    Network, networkId, selpalCellNumber, nationality, ethicGroup,
                    preferredLanguage, maritalStatus, residence, shopAddress, salesRepId, copyOfId,
                    proofOfResidence, userCode, pin);
            operator.setId(operatorId);

            service.updateOperator(operator);

            return new ResponseEntity(HttpStatus.OK);
        }
        catch(IOException e)
        {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Operator> getOperatorById(@PathVariable long id) throws OperatorNotFoundException {

        Operator operator = service.getOperatorById(id);
        if(operator != null) {
            return new ResponseEntity<Operator>(operator, HttpStatus.OK);
        }else{
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public ResponseEntity<List<Operator>> getAllOperators() {

        List<Operator> operatorList = service.getAllOperators();

        if(operatorList != null) {
            return new ResponseEntity<List<Operator>>(operatorList, HttpStatus.OK);
        }else{
            return  new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}



