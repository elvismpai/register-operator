package za.co.Infowarestudios.Register.Operator.exceptions;

/**
 * Created by SourceCode on 09/Dec/2015.
 */
public class OperatorNotFoundException extends Exception {
    public OperatorNotFoundException(String message) {
        super(message);
    }
}
