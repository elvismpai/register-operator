package za.co.Infowarestudios.Register.Operator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import za.co.Infowarestudios.Register.Operator.service.OperatorService;

@SpringBootApplication
public class RegisterOperatorApplication {

    @Bean
    OperatorService operatorService(){return new OperatorService();}

    public static void main(String[] args) {
        SpringApplication.run(RegisterOperatorApplication.class, args);
    }
}
