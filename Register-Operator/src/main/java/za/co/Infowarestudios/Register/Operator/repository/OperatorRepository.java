package za.co.Infowarestudios.Register.Operator.repository;

import org.springframework.data.repository.CrudRepository;
import za.co.Infowarestudios.Register.Operator.entity.Operator;


/**
 * Created by SourceCode on 09/Dec/2015.
 */
public interface OperatorRepository extends CrudRepository<Operator,Long>{

}

