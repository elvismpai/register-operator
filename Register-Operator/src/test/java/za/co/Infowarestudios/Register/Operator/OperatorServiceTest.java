package za.co.Infowarestudios.Register.Operator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import za.co.Infowarestudios.Register.Operator.exceptions.OperatorNotFoundException;
import za.co.Infowarestudios.Register.Operator.repository.OperatorRepository;
import za.co.Infowarestudios.Register.Operator.service.OperatorService;
import za.co.Infowarestudios.Register.Operator.entity.Operator;

import java.util.*;

/**
 * Created by SourceCode on 09/Dec/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RegisterOperatorApplication.class)
@WebAppConfiguration
public class OperatorServiceTest {
    @Autowired
    private OperatorRepository repository;

    @Autowired
    private OperatorService service;

    @Test
    public void testCreateOperator() {
        try {
            Operator data = new Operator("Elvis", "Mpai","EMP001", "7125025863084", "111",
                    "002555", "073349555", "0125588866",
                    "0115888368", "102 Park", "Parktown",
                    "Jzi", "2000", "mrEl@gmail.com", "MTN",
                    "22555444", "58555588",
                    "African", "Black", "en",
                    "Married", "Town", "Shop 201",
                    "00255", "valid", "valid",
                    "25554", "5632");

            Operator operator = service.createOperator(data);

            repository.save(data);
            assert (operator.equals(data));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void testUpdateOperator() throws OperatorNotFoundException {
        try {
            Operator data = new Operator("Elvis", "Mpai","EMP001", "7125025863084", "111",
                    "002555", "073349555", "0125588866",
                    "0115888368", "102 Park", "Parktown",
                    "Jzi", "2000", "mrEl@gmail.com", "MTN",
                    "22555444", "58555588",
                    "African", "Black", "en",
                    "Married", "Town", "Shop 201",
                    "00255", "valid", "valid",
                    "25554", "5632");

            Operator operator = service.createOperator(data);

            operator.setCellNumber("0715268864");
            operator.setNetwork("Vodacom");

            Operator updateOperator = service.updateOperator(operator);

            assert (operator.equals(updateOperator));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void testOperatorById() throws OperatorNotFoundException {
        try {
            Operator data = new Operator("Elvis", "Mpai","EMP001", "7125025863084", "111",
                    "002555", "073349555", "0125588866",
                    "0115888368", "102 Park", "Parktown",
                    "Jzi", "2000", "mrEl@gmail.com", "MTN",
                    "22555444", "58555588",
                    "African", "Black", "en",
                    "Married", "Town", "Shop 201",
                    "00255", "valid", "valid",
                    "25554", "5632");

            Operator operator = service.createOperator(data);

            Operator findOperatorById = service.getOperatorById(operator.getId());

            assert (operator.equals(findOperatorById));

        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void testGetAllOperators() throws OperatorNotFoundException {
        try {
            List<Operator> operatorList = service.getAllOperators();

            List<Operator> listOperator = new ArrayList<Operator>();
            for (Operator operator : repository.findAll()) {
                listOperator.add(operator);
            }
            assert (listOperator.size() == operatorList.size());
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
