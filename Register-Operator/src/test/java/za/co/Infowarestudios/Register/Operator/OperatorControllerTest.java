package za.co.Infowarestudios.Register.Operator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import za.co.Infowarestudios.Register.Operator.exceptions.OperatorNotFoundException;
import za.co.Infowarestudios.Register.Operator.service.OperatorService;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by SourceCode on 09/Dec/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RegisterOperatorApplication.class)
@WebAppConfiguration
public class OperatorControllerTest {
    private MockMvc mockMvc;

    private MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    OperatorService operatorService;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testCreateOperator() {
        try {
            final String json = "{\"firstName\":\"Elvis\",\"lastName\":\"Mpai\",\"idOrPassport\":\"5802045312089\"," +
                    "\"iccid\":\"8991101200003204510\",\"serialNumber\":\"899110121545304510\",\"cellNumber\":\"0732569854\"," +
                    "\"homeNumber\":\"0125663258\",\"workNumber\":\"0115634785\",\"streetAddress\":\"145 Zoon\"," +
                    "\"suburb\":\"Zulu\",\"cityOrTown\":\"Pretoria\",\"code\":\"1000\",\"email\":\"eeee@sddd.com\"," +
                    "\"network\":\"MTN\",\"networkId\":\"145236\",\"selpalCellNumber\":\"073655555\",\"nationality\":\"African\"," +
                    "\"ethicGroup\":\"Black\",\"preferredLanguage\":\"English\",\"maritialStaus\":\"Single\"," +
                    "\"residence\":\"xxxxx\",\"shopAddress\":\"Shop 15\",\"salesRepId\":\"00254\"," +
                    "\"copyOfId\":\"valid\",\"proofOfResidence\":\"valid\",\"userCode\":\"14586\",\"pin\":\"1536\"}";


            mockMvc.perform(put("/operators")
                    .contentType(APPLICATION_JSON_UTF8)
                    .content(json))
                    .andExpect(status().isCreated());
        } catch (Exception e) {
            e.getLocalizedMessage();
        }
    }

    @Test
    public void testUpdateOperator() throws OperatorNotFoundException {
        try {
            final String json = "{\"id\":\"1\",\"firstName\":\"Elvis\",\"lastName\":\"Mpai\",\"idOrPassport\":\"5802045312089\"," +
                    "\"iccid\":\"8991101200003204510\",\"serialNumber\":\"899110121545304510\",\"cellNumber\":\"0732569854\"," +
                    "\"homeNumber\":\"0125663258\",\"workNumber\":\"0115634785\",\"streetAddress\":\"145 Zoon\"," +
                    "\"suburb\":\"Zulu\",\"cityOrTown\":\"Pretoria\",\"code\":\"1000\",\"email\":\"eeee@sddd.com\"," +
                    "\"network\":\"MTN\",\"networkId\":\"145236\",\"selpalCellNumber\":\"073655555\",\"nationality\":\"African\"," +
                    "\"ethicGroup\":\"Black\",\"preferredLanguage\":\"English\",\"maritialStaus\":\"Single\"," +
                    "\"residence\":\"xxxxx\",\"shopAddress\":\"Shop 15\",\"salesRepId\":\"00254\"," +
                    "\"copyOfId\":\"valid\",\"proofOfResidence\":\"valid\",\"userCode\":\"14586\",\"pin\":\"1536\"}";

            mockMvc.perform(post("/operators")
                    .contentType(APPLICATION_JSON_UTF8)
                    .content(json))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.getMessage();
        }

    }

    @Test
    public void testGetOperatorById() throws OperatorNotFoundException {
        try {

            mockMvc.perform(get("/operators/{id}", 1L)
                    .contentType(APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Test
    public void testGetAllOperators(){
        try {
            mockMvc.perform(get("/operators")
                    .contentType(APPLICATION_JSON_UTF8))
                    .andExpect(status().isOk());
        }catch (Exception e){
            e.getMessage();
        }
    }

}
