package za.co.Infowarestudios.Register.Operator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import za.co.Infowarestudios.Register.Operator.entity.Operator;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by SourceCode on 12/18/15.
 */
@RunWith(MockitoJUnitRunner.class)
public class OperatorTest {
    @Mock
    Operator operator;
    @Before
    public  void setup(){

    }
    @Test
    public void testOperator(){

        when(operator.getId()).thenReturn(1L);
        when(operator.getFirstName()).thenReturn("Khomotjo");
        when(operator.getLastName()).thenReturn("Lebogo");
        when(operator.getOperatorId()).thenReturn("KLE001");
        when(operator.getIdOrPassport()).thenReturn("1892938475645");
        when(operator.getIccid()).thenReturn("3456765432189098");
        when(operator.getSerialNumber()).thenReturn("09844556677");
        when(operator.getCellNumber()).thenReturn("0876546676");
        when(operator.getHomeNumber()).thenReturn("0155050012");
        when(operator.getWorkNumber()).thenReturn("0114756555");
        when(operator.getStreetAddress()).thenReturn("49 new Road");
        when(operator.getSuburb()).thenReturn("Grand Central");
        when(operator.getCityOrTown()).thenReturn("Midrand");
        when(operator.getCode()).thenReturn("0615");
        when(operator.getEmail()).thenReturn("operator@gmail.com");
        when(operator.getNetwork()).thenReturn("goo");
        when(operator.getNetworkId()).thenReturn("3422");
        when(operator.getSelpalCellNumber()).thenReturn("22332");
        when(operator.getNationality()).thenReturn("African");
        when(operator.getEthicGroup()).thenReturn("Black");
        when(operator.getPreferredLanguage()).thenReturn("English");
        when(operator.getMaritalStatus()).thenReturn("Single");
        when(operator.getResidence()).thenReturn("485 Tembisa");
        when(operator.getShopAddress()).thenReturn("112 Tembisa");
        when(operator.getSalesRepId()).thenReturn("12322");
        when(operator.getCopyOfId()).thenReturn("copyOfID");
        when(operator.getProofOfResidence()).thenReturn("copyOfRes");
        when(operator.getUserCode()).thenReturn("code");
        when(operator.getPin()).thenReturn("pin");


        assertEquals(1L, operator.getId());
        assertEquals("Khomotjo", operator.getFirstName());
        assertEquals("Lebogo", operator.getLastName());
        assertEquals("KLE001",operator.getOperatorId());
        assertEquals("1892938475645",operator.getIdOrPassport());
        assertEquals("3456765432189098",operator.getIccid());
        assertEquals("09844556677",operator.getSerialNumber());
        assertEquals("0876546676",operator.getCellNumber());
        assertEquals("0155050012",operator.getHomeNumber());
        assertEquals("0114756555",operator.getWorkNumber());
        assertEquals("49 new Road",operator.getStreetAddress());
        assertEquals("Grand Central",operator.getSuburb());
        assertEquals("Midrand",operator.getCityOrTown());
        assertEquals("0615",operator.getCode());
        assertEquals("operator@gmail.com",operator.getEmail());
        assertEquals("goo",operator.getNetwork());
        assertEquals("3422",operator.getNetworkId());
        assertEquals("22332",operator.getSelpalCellNumber());
        assertEquals("African",operator.getNationality());
        assertEquals("Black",operator.getEthicGroup());
        assertEquals("English",operator.getPreferredLanguage());
        assertEquals("Single",operator.getMaritalStatus());
        assertEquals("485 Tembisa",operator.getResidence());
        assertEquals("112 Tembisa",operator.getShopAddress());
        assertEquals("12322",operator.getSalesRepId());
        assertEquals("copyOfID",operator.getCopyOfId());
        assertEquals("copyOfRes",operator.getProofOfResidence());
        assertEquals("code",operator.getUserCode());
        assertEquals("pin",operator.getPin());
    }
}
