package za.co.Infowarestudios.Register.Operator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RegisterOperatorApplication.class)
@WebAppConfiguration
public class RegisterOperatorApplicationTests {

	@Test
	public void contextLoads() {
	}

}
